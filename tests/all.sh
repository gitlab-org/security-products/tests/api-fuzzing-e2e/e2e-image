#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

FAILED=0
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

function assert_pass() {
  local ACT_RESULT=$1
  local LINE=$2

  if [[ -n "$LINE" ]]; then
    LINE_MSG="line $LINE: "
  else
    echo "Warning: line number not passed to assert function"
  fi

  if [[ $ACT_RESULT -ne 0 ]]; then

      FAILED=1

      echo
      echo "--------------------"
      echo -e "${RED}$SOURCE: ${LINE_MSG}Expected verify to pass, but it failed with code $ACT_RESULT${NC}"
      echo "--------------------"
  fi;
}

function assert_fail() {
  local ACT_RESULT=$1
  local LINE=$2

  if [[ -n "$LINE" ]]; then
    LINE_MSG="line $LINE: "
  else
    echo "Warning: line number not passed to assert function"
  fi

  if [[ $ACT_RESULT -eq 0 ]]; then

      FAILED=1

      echo
      echo "--------------------"
      echo -e "${RED}$SOURCE: ${LINE_MSG}Expected verify to fail, but it passed${NC}"
      echo "--------------------"
  fi;
}

function assert_eq() {
  local EXPECTED=$1
  local ACTUAL=$2
  local LINE=$3

  if [[ -n "$LINE" ]]; then
    LINE_MSG="line $LINE: "
  else
    echo "Warning: line number not passed to assert function"
  fi

  if [[ "$(cat $EXPECTED)" != "$(cat $ACTUAL)" ]]; then

      FAILED=1

      echo
      echo "--------------------"
      echo -e "${RED}$SOURCE: ${LINE_MSG}Expected values to match, but they are different.${NC}"
      echo "Actual output: $ACTUAL"
      echo "--------------------"
      echo diff $EXPECTED $ACTUAL
      diff $EXPECTED $ACTUAL
      echo

  else

    rm -f $ACTUAL

  fi;
}

SCRIPTS_DIR="${DIR}/../scripts"
VERIFY_SCRIPT_PATH="${SCRIPTS_DIR}/verify.sh"
VERIFY_RECORD_STATUS_SCRIPT_PATH="${SCRIPTS_DIR}/verify_record_status.sh"
TEST_REPORT_PATH="${DIR}/test_assets/test_report.json"

export E2E_VALIDATE_ASSETS=false
export EXPECT_REPORT_VERSION="14.0.4"
export CI_PROJECT_URL="https://example.com/gitlab-org/test_with_postman_collection_v2_1_tls_url_over_https_fuzzing"
export REPORT="${TEST_REPORT_PATH}" \

# Output buffering is disabled via stdbuf to ensure that the assertion message is at the end of the output

# ##########################################################################
# Should pass
# Expected CVEs: yes
# CWE: no
fileActualOutput=$(mktemp /tmp/verify.test1.XXXXXX)
EXPECT_REPORT_COUNT_MIN=3 \
EXPECT_REPORT_COUNT_MAX=3 \
EXPECT_REPORT_CVE_0="bucket:check:DNS Rebinding:check_arg::assert:DNS Rebinding" \
EXPECT_REPORT_CVE_1="bucket:param:Header: authorization:check:Authentication Token:check_arg::assert:Authentication Token:data:4960d0f6adc260d6a568516c17a49bdb194b74799e84755900d40022ba394969" \
EXPECT_REPORT_CVE_2="bucket:check:Application Information Disclosure:check_arg::assert:Response Body Analysis" \
stdbuf -i0 -o0 -e0 "${VERIFY_SCRIPT_PATH}" > $fileActualOutput
rc=$?
cat $fileActualOutput
assert_pass $rc $LINENO
assert_eq "$DIR/test_assets/actual_1.console" "$fileActualOutput" $LINENO

# ##########################################################################
# Should pass
# Expected CVEs: no
# CWE: no
fileActualOutput=$(mktemp /tmp/verify.test2.XXXXXX)
EXPECT_REPORT_COUNT_MIN=3 \
EXPECT_REPORT_COUNT_MAX=3 \
stdbuf -i0 -o0 -e0 "${VERIFY_SCRIPT_PATH}" > $fileActualOutput
rc=$?
cat $fileActualOutput
assert_pass $rc $LINENO
assert_eq "$DIR/test_assets/actual_2.console" "$fileActualOutput" $LINENO

# ##########################################################################
# Should fail - incorrect counts
# Expected CVEs: no
# CWE: no
fileActualOutput=$(mktemp /tmp/verify.test3.XXXXXX)
EXPECT_REPORT_COUNT_MIN=1 \
EXPECT_REPORT_COUNT_MAX=2 \
stdbuf -i0 -o0 -e0 "${VERIFY_SCRIPT_PATH}" > $fileActualOutput
rc=$?
cat $fileActualOutput
assert_fail $rc $LINENO
assert_eq "$DIR/test_assets/actual_3.console" "$fileActualOutput" $LINENO

# ##########################################################################
# Should fail - incorrect CVEs
# Expected CVEs: yes
# CWE: no
fileActualOutput=$(mktemp /tmp/verify.test4.XXXXXX)
EXPECT_REPORT_COUNT_MIN=3 \
EXPECT_REPORT_COUNT_MAX=3 \
EXPECT_REPORT_CVE_0="incorrect cve" \
EXPECT_REPORT_CVE_1="bucket:param:Header: authorization:check:Authentication Token:check_arg::assert:Authentication Token:data:4960d0f6adc260d6a568516c17a49bdb194b74799e84755900d40022ba394969" \
EXPECT_REPORT_CVE_2="bucket:check:Application Information Disclosure:check_arg::assert:Response Body Analysis" \
stdbuf -i0 -o0 -e0 "${VERIFY_SCRIPT_PATH}" > $fileActualOutput
rc=$?
cat $fileActualOutput
assert_fail $rc $LINENO
assert_eq "$DIR/test_assets/actual_4.console" "$fileActualOutput" $LINENO

# ##########################################################################
# Should pass
# Expected CVEs: yes
# CWE: Yes
fileActualOutput=$(mktemp /tmp/verify.test5.XXXXXX)
CWE=cwe_89 \
EXPECT_REPORT_COUNT_cwe_89_MIN=3 \
EXPECT_REPORT_COUNT_cwe_89_MAX=3 \
EXPECT_REPORT_CVE_cwe_89_0="bucket:check:DNS Rebinding:check_arg::assert:DNS Rebinding" \
EXPECT_REPORT_CVE_cwe_89_1="bucket:param:Header: authorization:check:Authentication Token:check_arg::assert:Authentication Token:data:4960d0f6adc260d6a568516c17a49bdb194b74799e84755900d40022ba394969" \
EXPECT_REPORT_CVE_cwe_89_2="bucket:check:Application Information Disclosure:check_arg::assert:Response Body Analysis" \
stdbuf -i0 -o0 -e0 "${VERIFY_SCRIPT_PATH}" > $fileActualOutput
rc=$?
cat $fileActualOutput
assert_pass $rc $LINENO
assert_eq "$DIR/test_assets/actual_5.console" "$fileActualOutput" $LINENO

# ##########################################################################
# Should pass
# Expected CVEs: optional
# CWE: no
fileActualOutput=$(mktemp /tmp/verify.test6.XXXXXX)
EXPECT_REPORT_COUNT_MIN=0 \
EXPECT_REPORT_COUNT_MAX=3 \
EXPECT_REPORT_CVE_OPTIONAL_0="bucket:check:DNS Rebinding:check_arg::assert:DNS Rebinding" \
EXPECT_REPORT_CVE_OPTIONAL_1="bucket:param:Header: authorization:check:Authentication Token:check_arg::assert:Authentication Token:data:4960d0f6adc260d6a568516c17a49bdb194b74799e84755900d40022ba394969" \
EXPECT_REPORT_CVE_OPTIONAL_2="bucket:check:Application Information Disclosure:check_arg::assert:Response Body Analysis" \
stdbuf -i0 -o0 -e0 "${VERIFY_SCRIPT_PATH}" > $fileActualOutput
rc=$?
cat $fileActualOutput
assert_pass $rc $LINENO
assert_eq "$DIR/test_assets/actual_6.console" "$fileActualOutput" $LINENO

# ##########################################################################
# Should pass
# Expected CVEs: yes
# CWE: Yes
fileActualOutput=$(mktemp /tmp/verify.test7.XXXXXX)
CWE=cwe_89 \
EXPECT_REPORT_COUNT_cwe_89_MIN=0 \
EXPECT_REPORT_COUNT_cwe_89_MAX=3 \
EXPECT_REPORT_CVE_OPTIONAL_cwe_89_0="bucket:check:DNS Rebinding:check_arg::assert:DNS Rebinding" \
EXPECT_REPORT_CVE_OPTIONAL_cwe_89_1="bucket:param:Header: authorization:check:Authentication Token:check_arg::assert:Authentication Token:data:4960d0f6adc260d6a568516c17a49bdb194b74799e84755900d40022ba394969" \
EXPECT_REPORT_CVE_OPTIONAL_cwe_89_2="bucket:check:Application Information Disclosure:check_arg::assert:Response Body Analysis" \
stdbuf -i0 -o0 -e0 "${VERIFY_SCRIPT_PATH}" > $fileActualOutput
rc=$?
cat $fileActualOutput
assert_pass $rc $LINENO
assert_eq "$DIR/test_assets/actual_7.console" "$fileActualOutput" $LINENO

# ##########################################################################
# Should fail - incorrect optional CVEs
# Expected CVEs: optional
# CWE: no
fileActualOutput=$(mktemp /tmp/verify.test8.XXXXXX)
EXPECT_REPORT_COUNT_MIN=0 \
EXPECT_REPORT_COUNT_MAX=3 \
EXPECT_REPORT_CVE_OPTIONAL_0="incorrect cve" \
EXPECT_REPORT_CVE_OPTIONAL_1="bucket:param:Header: authorization:check:Authentication Token:check_arg::assert:Authentication Token:data:4960d0f6adc260d6a568516c17a49bdb194b74799e84755900d40022ba394969" \
EXPECT_REPORT_CVE_OPTIONAL_2="bucket:check:Application Information Disclosure:check_arg::assert:Response Body Analysis" \
stdbuf -i0 -o0 -e0 "${VERIFY_SCRIPT_PATH}" > $fileActualOutput
rc=$?
cat $fileActualOutput
assert_fail $rc $LINENO
assert_eq "$DIR/test_assets/actual_8.console" "$fileActualOutput" $LINENO

# ##########################################################################
# Should pass - matching success status codes
fileActualOutput=$(mktemp /tmp/verifyRecordStatus.test9.XXXXXX)
DAST_API_SUCCESS_STATUS_CODES="200, 201, 204, 404" \
EXPECT_MESSAGE="Testing completed successfully" \
EXPECT_SUCCESS=1 \
JOB_STATUS=1 \
stdbuf -i0 -o0 -e0 "${VERIFY_RECORD_STATUS_SCRIPT_PATH}" > $fileActualOutput
rc=$?
cat $fileActualOutput
assert_pass $rc $LINENO
assert_eq "$DIR/test_assets/record_status/actual_1.console" "$fileActualOutput" $LINENO

# ##########################################################################
# Should pass - non-matching success status codes
fileActualOutput=$(mktemp /tmp/verifyRecordStatus.test10.XXXXXX)
DAST_API_SUCCESS_STATUS_CODES="200, 201, 204, 404" \
EXPECT_MESSAGE="Testing failed: Initial request to DELETE http://target:7777/api/users/2 failed with status code 401" \
EXPECT_SUCCESS=0 \
JOB_STATUS=0 \
stdbuf -i0 -o0 -e0 "${VERIFY_RECORD_STATUS_SCRIPT_PATH}" > $fileActualOutput
rc=$?
cat $fileActualOutput
assert_pass $rc $LINENO
assert_eq "$DIR/test_assets/record_status/actual_2.console" "$fileActualOutput" $LINENO

# ##########################################################################

if [[ $FAILED -eq 0 ]]; then
  echo
  echo "--------------------"
  echo -e "${GREEN}All tests passed${NC}"
  echo "--------------------"

  exit 0
fi

echo
echo "--------------------"
echo -e "${RED}Some tests failed${NC}"
echo "--------------------"

exit 1
