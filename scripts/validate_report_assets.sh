#!/bin/bash

set -ex

assert() {
    echo "* assert $2"
    eval $2
    local rc=$?
    if [[ $rc -ne 0 ]]; then
        echo "FAIL: $1 $2"
        exit 1
    fi
}

download_asset() {
    local url=$1
    local filename=$2
    local artifact_job_id=${url##*"/-/jobs/"}
    local artifact_job_id=${artifact_job_id%%"/artifacts/"*}
    local artifact_path=${url##*"/artifacts/file/"}
    local artifact_path=${artifact_path%%\?*}
    local artifact_api_url=$CI_API_V4_URL/projects/$CI_PROJECT_ID/jobs/$artifact_job_id/artifacts/$artifact_path

    curl --location -H "JOB-TOKEN: $CI_JOB_TOKEN" "$artifact_api_url" --output "$filename"

    rc=$?
    assert "download asset $artifact_api_url" "[[ $rc -eq 0 ]]"
}

get_asset_url() {
    local vuln_index=$1
    local asset_index=$2
    local report=$3

    local url=$(jq ".vulnerabilities[$vuln_index] | .assets[$asset_index] | .url" "$report")
    local url=${url%%\"}
    local url=${url##\"}
    echo "$url"
}

get_asset_filename() {
    local vuln_index=$1
    local asset_index=$2
    local report=$3

    local url=$(jq ".vulnerabilities[$vuln_index] | .assets[$asset_index] | .url" "$report")
    local filename=${url##*\/}
    local filename=${filename%%\?*}
    echo "$filename"
}

verify_http_messages() {
    local file_path=$1
    echo "verify_http_messages: $file_path"
    local file_mime_type=`file -b --mime-type "$file_path"`
    assert "check mime-type for $file_path" "[[ \"$file_mime_type\" == \"application/zip\" ]]"
}

verify_postman() {
    echo "verify_postman: $1"
    local postman="$1"
    local schema=$(jq '.info.schema' "$postman")
    assert "$TEST verify $1 schema" "[[ \"$schema\" == \"https://schema.getpostman.com/json/collection/v2.0.0/collection.json\" ]]"
    local item_cnt=$(jq '.item | length' "$postman")
    assert "$TEST verify $1 item count" "[[ $item_cnt -eq 1 ]]"
}

verify_asset_url(){
    echo "verify url asset prefix"
    local url=$1
    local expected_prefix="$CI_PROJECT_URL"
    local received_prefix=${url%%"/-/jobs/"*}
    assert "$TEST verify $1 prefix" "[[ \"$expected_prefix\" == \"$received_prefix\" ]]"
}

validate_downloads(){
    local report=$1
    local asset_temp_path="tmp_assets"

    # set -ex

    echo "report: $report"
    echo "asset_path: $asset_temp_path"

    mkdir -p $asset_temp_path

    vuln_cnt=$(jq '.vulnerabilities | length' "$report")
    echo "vuln_cnt: $vuln_cnt"

    if [[ "${E2E_VALIDATE_ASSETS:-true}" != "true" ]]; then
        echo "Skipping validate assets"
        exit 0
    fi

    for ((vuln_index = 0 ; vuln_index < $vuln_cnt ; vuln_index++ )); do
        echo "for vuln_index: $vuln_index"

        asset_cnt=$(jq ".vulnerabilities[$vuln_index] | .assets | length" "$report")
        echo "asset_cnt: $asset_cnt"

        for ((asset_index = 0 ; asset_index < $asset_cnt ; asset_index++ )); do
            asset_url=$(get_asset_url $vuln_index $asset_index $report)
            filename=$(get_asset_filename $vuln_index $asset_index $report)
            asset_filename=$asset_temp_path/$filename

            verify_asset_url "$asset_url"
            download_asset "$asset_url" "$asset_filename"

            if [[ "${filename##*.}" == "zip" ]]; then
                verify_http_messages "$asset_filename"
            fi

            if [[ "${filename##*.}" == "json" ]]; then
                verify_postman "$asset_filename"
            fi
        done
    done
}

validate_downloads "$1"

# ----------------------------------------

# end
