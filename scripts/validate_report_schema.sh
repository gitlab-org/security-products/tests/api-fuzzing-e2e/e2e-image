#!/bin/bash

set -ex

assert() {
    echo "* assert $2"
    eval $2
    local rc=$?
    if [[ $rc -ne 0 ]]; then
        echo "FAIL: $1 $2"
        exit 1
    fi
}


validate_schema(){
    local report=$1
    local expect_report_version=$2
    local rc=0
    
    local schema_temp_path="tmp_schema"
    mkdir -p $schema_temp_path

    local schema=$schema_temp_path/schema.json
    local schema_url=$(jq '.schema' $report)
    schema_url=${schema_url/\/blob\//\/raw\/}
    schema_url=${schema_url%%\"}
    schema_url=${schema_url##\"}
    
    curl $schema_url --output $schema --silent
    rc=$?
    assert "download schema $schema_url" "[[ $rc -eq 0 ]]"

    local schema_version=$(jq '.self.version' $schema)
    local report_version=$(jq '.version' $report)

    assert "match report version:$report_version and expected report version:$schema_version" "[[ $report_version == $expect_report_version ]]"    
    assert "match report version:$report_version and schema version:$schema_version" "[[ $report_version == $schema_version ]]"

    yajsv -s $schema $report
    rc=$?
    assert "schema validation" "[[ $rc -eq 0 ]]"
}

validate_schema "$1" "$2"

# ----------------------------------------

# end
