#!/bin/bash

# Verify response status code from the inital request response during the record phase
# matches any of the codes defined in the user-defined success status codes.

# Upstream job definition type 1:
#    variables:
#      EXPECT_SUCCESS: 1
#      EXPECT_MESSAGE: "Testing completed successfully"
#      DAST_API_SUCCESS_STATUS_CODES: "200, 201, 204, 404"
#    asset: assets/with_token.har
#
# Upstream job definition type 2:
#    variables:
#      EXPECT_SUCCESS: 0
#      EXPECT_MESSAGE: "Testing failed: Initial request to DELETE http://target:7777/api/users?user=dd failed with status code 401"
#      DAST_API_SUCCESS_STATUS_CODES: "200, 201, 204, 404"
#    asset: assets/no_token.har
#

if [[ -z "$EXPECT_SUCCESS" ]]; then
    echo "Error, missing variable EXPECT_SUCCESS"
    exit 1
fi
if [[ -z "$EXPECT_MESSAGE" ]]; then
    echo "Error, missing variable EXPECT_MESSAGE"
    exit 1
fi
if [[ -z "$DAST_API_SUCCESS_STATUS_CODES" ]]; then
    echo "Error, missing variable DAST_API_SUCCESS_STATUS_CODES"
    exit 1
fi

JQ=/usr/bin/jq

job_status=""
filePipelineJobs="dast-record-status-pipeline-jobs.log"
fileJobLog="dast-api-job.log"
TEST_JOB_LOG_SUCCESS=""
TEST_JOB_LOG_FAILURE=""

if [[ -n $JOB_STATUS ]]; then
  # ##########################################################################
  # For testing purposes in tests/all.sh
  # We reference defined test job status and test job log files

  if [[ $JOB_STATUS -eq 1 ]]; then
    job_status="success"
    TEST_JOB_LOG_SUCCESS="${DIR}/../tests/test_assets/record_status/test_job_log_success.console"
    cat $TEST_JOB_LOG_SUCCESS > $fileJobLog
    if [[ $? -ne 0 ]]; then
      echo "Error: Failed to retrieve test job log"

      exit 1
    fi
  else
    job_status="failed"
    TEST_JOB_LOG_FAILURE="${DIR}/../tests/test_assets/record_status/test_job_log_failure.console"
    cat $TEST_JOB_LOG_FAILURE > $fileJobLog
    if [[ $? -ne 0 ]]; then
      echo "Error: Failed to retrieve test job log"

      exit 1
    fi
  fi
else
  # ##########################################################################
  # Using GitLab API, fetch pipeline jobs to get dast_api job id and job status

  if [ -z "$APISECURITY_E2E_RO_PROJECT_TOKEN" ]; then
    echo "Missing 'APISECURITY_E2E_RO_PROJECT_TOKEN' environment variable."
    exit 1
  fi

  STATUS_CODE=$(curl \
    --write-out "%{response_code}" \
    --output $filePipelineJobs \
    -H "PRIVATE-TOKEN: $APISECURITY_E2E_RO_PROJECT_TOKEN" \
    "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}/jobs")
  RC=$?
  if [[ "$RC" -ne "0" ]]; then
    echo "Error, Unable to retrieve pipeline jobs, error calling GitLab API";
    echo "Error, curl exited with a non-zero code: $RC";
    echo "See docs for more information: https://everything.curl.dev/cmdline/exitcode.html"

    exit 1
  fi
  if [[ "$STATUS_CODE" -ne "200" ]]; then
    echo "Error, Unable to retrieve pipeline jobs, error calling GitLab API";
    echo "Error, Status Code was $STATUS_CODE, but wanted 200";
    echo "Response body:"
    cat $filePipelineJobs

    exit 1
  fi

  # Check API response for errors: {"error":"message",....}
  set +e
  grep -q -e '"error"' $filePipelineJobs
  RC=$?
  set -e
  if [[ "$RC" -eq "0" ]]; then
    echo "Error, Unable to retrieve pipeline jobs, error returned by GitLab API:"
    cat $filePipelineJobs

    exit 1
  fi

  job_id=$(cat $filePipelineJobs | jq -r '.[] | select(.name == "dast_api") | .id')
  job_status=$(cat $filePipelineJobs | jq -r '.[] | select(.name == "dast_api") | .status')

  # ##########################################################################
  # Using GitLab API, fetch pipeline job log for the dast_api job

  STATUS_CODE=$(curl \
    --location \
    --write-out "%{response_code}" \
    --output $fileJobLog \
    -H "PRIVATE-TOKEN: $APISECURITY_E2E_RO_PROJECT_TOKEN" \
    "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/jobs/$job_id/trace")
  RC=$?
  if [[ "$RC" -ne "0" ]]; then
    echo "Error, Unable to retrieve job log, error calling GitLab API";
    echo "Error, curl exited with a non-zero code: $RC";
    echo "See docs for more information: https://everything.curl.dev/cmdline/exitcode.html"

    exit 1
  fi
  if [[ "$STATUS_CODE" -ne "200" ]]; then
    echo "Error, Unable to retrieve job log, error calling GitLab API";
    echo "Error, Status Code was $STATUS_CODE, but wanted 200";
    echo "Response body:"
    cat $fileJobLog

    exit 1
  fi

  # Check API response for errors: {"error":"message",....}
  set +e
  grep -q -e '"error"' $fileJobLog
  RC=$?
  set -e
  if [[ "$RC" -eq "0" ]]; then
    echo "Error, Unable to retrieve job log, error returned by GitLab API:"
    cat $fileJobLog

    exit 1
  fi
fi

GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

# ##########################################################################
# check pipeline job status, and match with expected success/failure message in log output
#

if [[ $job_status = "success" ]]; then
  result=$(grep "$EXPECT_MESSAGE\$" $fileJobLog | sed 's/.*\[INF\][^:]*: //')
  if [[ $EXPECT_SUCCESS -eq 1 && $result > /dev/null ]]; then
    echo
    echo -e "${GREEN}Passed${NC} - Response codes match expected success status codes - ${DAST_API_SUCCESS_STATUS_CODES}."
    echo "|  Wanted message - ${EXPECT_MESSAGE}"
    echo "|"
    echo "| vv Got: vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv"
    echo "| $result"
    echo

    exit 0
  else

    echo -e "${RED}Error${NC}: Failed to retrieve expected message ${EXPECT_MESSAGE}, job status is ${job_status}"
    echo "Filtered result: $result"
    echo "Expected success status codes - ${DAST_API_SUCCESS_STATUS_CODES}"
    echo "Expected success - ${EXPECT_SUCCESS}"

    exit 1
  fi
elif [[ $job_status = "failed" ]]; then
  result=$(grep "$EXPECT_MESSAGE" $fileJobLog | sed 's/.*\[ERR\][^:]*: //')
  if [[ $EXPECT_SUCCESS -eq 0 && $result > /dev/null ]]; then

    echo
    echo -e "${RED}Failed${NC} - Response code does not match expected success status codes - ${DAST_API_SUCCESS_STATUS_CODES}."
    echo "|  Wanted message - ${EXPECT_MESSAGE}"
    echo "|"
    echo "| vv Got: vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv"
    echo "| $result"
    echo

    exit 0
  else
    echo -e "${RED}Error${NC}: Failed to retrieve expected message ${EXPECT_MESSAGE}, job status is ${job_status}"
    echo "Filtered result: $result"
    echo "Expected success status codes - ${DAST_API_SUCCESS_STATUS_CODES}"
    echo "Expected success - ${EXPECT_SUCCESS}"

    exit 1
  fi
else

  echo
  echo -e "${RED}Failed${NC} - Job status does not match success or failed."
  echo "|  Wanted job status success or failed, got ${job_status}"
  echo

  exit 1
fi

# ----------------------------------------

# end