#!/bin/bash

# Verify expected vulnerabilities match those in a report
#
# Upstream job definition type 1:
#    variables:
#      EXPECT_REPORT_CVE_0: "bucket:param:Path: /api/users:check:Generic Fuzzing:check_arg::assert:Status Code:data:7dfb4cf67742cb0660305e56ef816c53fcec892cae7f6ee39b75f34e659d672c"
#      EXPECT_REPORT_CVE_1: "bucket:param:Path: /api/users/2:check:Generic Fuzzing:check_arg::assert:Response Body Analysis:data:1113193d7e8884ae4c56395d59fafbee5c2e39a9ca672e259c25c92a5876ef69"
#      EXPECT_REPORT_CVE_2: "bucket:param:Body: first:check:JSON Fuzzing:check_arg::assert:Status Code:data:73b94a8ddf2cb06f9795f38faead5d3d3de7042f0285586efd9d75c3e53c3617"
#      EXPECT_REPORT_CVE_3: "bucket:param:Body: last:check:JSON Fuzzing:check_arg::assert:Status Code:data:0533befe9c0c9f4321d99679b042da568d0f2d52a3708900682e999772d55193"
#      EXPECT_REPORT_CVE_4: "bucket:param:Body: password:check:JSON Fuzzing:check_arg::assert:Status Code:data:f6a66393ef3cea6717c9f2a71d8547d6aa37ffaac881e79c346d8bd7addb02df"
#      EXPECT_REPORT_COUNT_MIN: 5
#      EXPECT_REPORT_COUNT_MAX: 6
#
# Upstream job definition type 2 (matrix):
#
# variables:
#     #
#     CWE: $CWE
#     #
#     EXPECT_REPORT_COUNT_cwe_22_MIN: 9
#     EXPECT_REPORT_COUNT_cwe_22_MAX: 9
#     EXPECT_REPORT_CVE_cwe_22_0: "bucket:check:DNS Rebinding:check_arg::assert:DNS Rebinding"
#     EXPECT_REPORT_CVE_cwe_22_1: "bucket:check:Application Information Disclosure:check_arg::assert:Response Body Analysis"
#     EXPECT_REPORT_CVE_cwe_22_2: "bucket:check:Application Information Disclosure:check_arg::assert:Response Body Analysis"
#     EXPECT_REPORT_CVE_cwe_22_3: "bucket:check:Application Information Disclosure:check_arg::assert:Response Body Analysis"
#     EXPECT_REPORT_CVE_cwe_22_4: "bucket:check:Application Information Disclosure:check_arg::assert:Response Body Analysis"
#     EXPECT_REPORT_CVE_cwe_22_5: "bucket:param:Query: vinput:check:Path Traversal:check_arg::assert:Response Body Analysis"
#     EXPECT_REPORT_CVE_cwe_22_6: "bucket:param:Query: vinput:check:Path Traversal:check_arg::assert:Response Body Analysis"
#     EXPECT_REPORT_CVE_cwe_22_7: "bucket:param:Query: vinput:check:Path Traversal:check_arg::assert:Response Body Analysis"
#     EXPECT_REPORT_CVE_cwe_22_8: "bucket:param:Query: vinput:check:Path Traversal:check_arg::assert:Response Body Analysis"
#     #
#     EXPECT_REPORT_COUNT_cwe_78_MIN: 3
#     EXPECT_REPORT_COUNT_cwe_78_MAX: 3
#     EXPECT_REPORT_CVE_cwe_78_0: "bucket:check:DNS Rebinding:check_arg::assert:DNS Rebinding"
#     EXPECT_REPORT_CVE_cwe_78_1: "bucket:param:Query: vinput:check:OS Command Injection:check_arg::assert:Response Body Analysis"
#     EXPECT_REPORT_CVE_cwe_78_2: "bucket:param:Query: vinput:check:OS Command Injection:check_arg::assert:Blind Injection"

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

# ##########################################################################
# Check for any errors in the logs and exit if found

grep -e "\[(ERR|CRT)\]" *.log &> /dev/null
if [[ $? -eq 0 ]]; then
    grep -e "\[(ERR|CRT)\]" *.log
    exit 1
fi
grep "Unhandled exception" *.log &> /dev/null
if [[ $? -eq 0 ]]; then
    grep "Unhandled exception" *.log &> /dev/null
    exit 1
fi

# ##########################################################################
# Handle check our expected variables

if [[ -n "$CWE" ]]; then
    CWE_PART="${CWE}_"
    EXPECT_REPORT_COUNT_MIN=$(eval "echo \$EXPECT_REPORT_COUNT_${CWE}_MIN")
    EXPECT_REPORT_COUNT_MAX=$(eval "echo \$EXPECT_REPORT_COUNT_${CWE}_MAX")
fi

# Collect expected CVEs from individual variables into EXPECT_CVEs array
EXPECT_CVE_COUNT=0
EXPECTED_CVES_REQUIRED=()
EXPECT_REPORT_CVE_NAME=EXPECT_REPORT_CVE_${CWE_PART}${EXPECT_CVE_COUNT}

while [[ -n "${!EXPECT_REPORT_CVE_NAME}" ]]
do
    EXPECTED_CVES_REQUIRED=("${EXPECTED_CVES_REQUIRED[@]}" "${!EXPECT_REPORT_CVE_NAME}" )
    EXPECT_CVE_COUNT=$(($EXPECT_CVE_COUNT + 1))
    EXPECT_REPORT_CVE_NAME=EXPECT_REPORT_CVE_${CWE_PART}${EXPECT_CVE_COUNT}
done

EXPECT_CVE_COUNT=0
EXPECTED_CVES_OPTIONAL=()
EXPECT_REPORT_CVE_NAME=EXPECT_REPORT_CVE_OPTIONAL_${CWE_PART}${EXPECT_CVE_COUNT}

while [[ -n "${!EXPECT_REPORT_CVE_NAME}" ]]
do
    EXPECTED_CVES_OPTIONAL=("${EXPECTED_CVES_OPTIONAL[@]}" "${!EXPECT_REPORT_CVE_NAME}" )
    EXPECT_CVE_COUNT=$(($EXPECT_CVE_COUNT + 1))
    EXPECT_REPORT_CVE_NAME=EXPECT_REPORT_CVE_OPTIONAL_${CWE_PART}${EXPECT_CVE_COUNT}
done

REPORTJSON="$REPORT"

if [[ -z "$REPORTJSON" ]]; then
    echo "Error, missing variable REPORT"
    exit 1
fi
if [[ -z "$EXPECT_REPORT_COUNT_MIN" ]]; then
    echo "Error, missing variable EXPECT_REPORT_COUNT_MIN"
    exit 1
fi
if [[ -z "$EXPECT_REPORT_COUNT_MAX" ]]; then
    echo "Error, missing variable EXPECT_REPORT_COUNT_MAX"
    exit 1
fi
if [[ -z "$EXPECT_REPORT_VERSION" ]]; then
    echo "Error, missing variable EXPECT_REPORT_VERSION"
    exit 1
fi

if [[ ! -e "$REPORTJSON" ]]; then
    echo "Error, '$REPORTJSON' doesn't exist."
    exit 1
fi

JQ=/usr/bin/jq
REPORT_COUNT=$($JQ '.vulnerabilities | length' "$REPORT")

# ## Skip status code assertions and sort ##########################################

# Mike (2022/07/25): Status code assertions are even less reliable in concurrent mode
#                    becuase resources can be deleted as we are using them.
# Mike (2022/08/04): Sort the values to make comparison easier and not dependent
#                    on order of being found/reported.

fileExpectedRequired=$(mktemp /tmp/checkreportfaultcount.ex.XXXXXX)
fileExpectedRequired2=$(mktemp /tmp/checkreportfaultcount.ex.XXXXXX)
fileExpectedOptional=$(mktemp /tmp/checkreportfaultcount.op.XXXXXX)
fileExpectedOptional2=$(mktemp /tmp/checkreportfaultcount.op.XXXXXX)
fileActual=$(mktemp /tmp/checkreportfaultcount.ac.XXXXXX)
fileActual2=$(mktemp /tmp/checkreportfaultcount.ac.XXXXXX)

# Remove status code assertions
# Remove `:data:xxxx` suffix from API Fuzzing results
# Sort findings

for ((i = 0 ; i < ${#EXPECTED_CVES_REQUIRED[@]} ; i++)); do
    echo ${EXPECTED_CVES_REQUIRED[$i]} >> $fileExpectedRequired
done

cat $fileExpectedRequired \
    | grep -v ":assert:Status Code:" - \
    | sed -E 's/:data:[a-f0-9]+//' \
    | sort - \
    > $fileExpectedRequired2

for ((i = 0 ; i < ${#EXPECTED_CVES_OPTIONAL[@]} ; i++)); do
    echo ${EXPECTED_CVES_OPTIONAL[$i]} >> $fileExpectedOptional
done

cat $fileExpectedOptional \
    | grep -v ":assert:Status Code:" - \
    | sed -E 's/:data:[a-f0-9]+//' \
    | sort - \
    > $fileExpectedOptional2

$JQ -n -f "$REPORTJSON" | $JQ '.vulnerabilities[].identifiers[0].value' | tr -d '"' \
    | grep -v ":assert:Status Code:" - \
    | sed -E 's/:data:[a-f0-9]+//' \
    | sort - \
    > $fileActual2

# Read files back in as arrays

IFS=$'\r\n' GLOBIGNORE='*' command eval 'EXPECTED_CVES_REQUIRED=($(cat $fileExpectedRequired2))'
IFS=$'\r\n' GLOBIGNORE='*' command eval 'EXPECTED_CVES_OPTIONAL=($(cat $fileExpectedOptional2))'
IFS=$'\r\n' GLOBIGNORE='*' command eval 'ACTUAL_CVES=($(cat $fileActual2))'
IFS=$'\r\n' GLOBIGNORE='*' command eval 'ACTUAL_CVES_FULL=($(cat $fileActual2))'

# Get the final count of things

ACTUAL_COUNT_FULL=${#ACTUAL_CVES_FULL[@]}
FAULT_COUNT_REQUIRED=${#EXPECTED_CVES_REQUIRED[@]}
FAULT_COUNT_OPTIONAL=${#EXPECTED_CVES_OPTIONAL[@]}
FAULT_COUNT_TOTAL=$(($FAULT_COUNT_REQUIRED + $FAULT_COUNT_OPTIONAL))
FAULT_COUNT_MIN=$FAULT_COUNT_REQUIRED
FAULT_COUNT_MAX=$FAULT_COUNT_TOTAL

# Remove any optional cves from actual cves array
for ((i = 0 ; i < ${#EXPECTED_CVES_OPTIONAL[@]} ; i++)); do

    rm -f $fileActual $fileActual2
    for ((j = 0 ; j < ${#ACTUAL_CVES[@]} ; j++)); do
        echo ${ACTUAL_CVES[$j]} >> $fileActual
    done

    cat $fileActual \
        | grep -v "${EXPECTED_CVES_OPTIONAL[$i]}" - \
        > $fileActual2

    IFS=$'\r\n' GLOBIGNORE='*' command eval  'ACTUAL_CVES=($(cat $fileActual2))'

done

rm -f \
    $fileExpectedRequired \
    $fileExpectedRequired2 \
    $fileExpectedOptional \
    $fileExpectedOptional2 \
    $fileActual \
    $fileActual2

# ##################################################################################

GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

if [[ $FAULT_COUNT_TOTAL -eq 0 ]]; then
    if [[ $REPORT_COUNT -ge $EXPECT_REPORT_COUNT_MIN && $REPORT_COUNT -le $EXPECT_REPORT_COUNT_MAX ]]; then

        echo
        echo -e "${GREEN}Passed${NC} - Vulnerabilities from $REPORTJSON match expected."
        echo "|  Wanted between $EXPECT_REPORT_COUNT_MIN and $EXPECT_REPORT_COUNT_MAX, got $REPORT_COUNT"
        echo "|"
        echo "| vv Actual: vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv"
        for ((i = 0 ; i < ${#ACTUAL_CVES_FULL[@]} ; i++)); do
            echo "| ${ACTUAL_CVES_FULL[$i]}"
        done
        echo

        exit 0
    else

        echo
        echo -e "${RED}Failed${NC} - Vulnerabilities from $REPORTJSON do not match expected."
        echo "|  Wanted between $EXPECT_REPORT_COUNT_MIN and $EXPECT_REPORT_COUNT_MAX, got $REPORT_COUNT"
        echo "|"
        echo "| vv Actual: vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv"
        for ((i = 0 ; i < ${#ACTUAL_CVES_FULL[@]} ; i++)); do
            echo "| ${ACTUAL_CVES_FULL[$i]}"
        done
        echo

        exit 1
    fi
fi

if [[ "${EXPECTED_CVES_REQUIRED[@]}" == "${ACTUAL_CVES[@]}" ]]; then

    echo
    echo -e "${GREEN}Passed${NC} - Vulnerabilities from $REPORTJSON match expected."
    echo "|  Wanted between $FAULT_COUNT_MIN and $FAULT_COUNT_MAX, got $ACTUAL_COUNT_FULL"
    echo "|"
    echo "| vv Required: vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv"
    for ((i = 0 ; i < ${#EXPECTED_CVES_REQUIRED[@]} ; i++)); do
        echo "| ${EXPECTED_CVES_REQUIRED[$i]}"
    done
    if [[ ${#EXPECTED_CVES_REQUIRED[@]} -eq 0 ]]; then
        echo "| No required vulnerabilities"
    fi
    echo "|"
    echo "| vv Optional: vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv"
    for ((i = 0 ; i < ${#EXPECTED_CVES_OPTIONAL[@]} ; i++)); do
        echo "| ${EXPECTED_CVES_OPTIONAL[$i]}"
    done
    if [[ ${#EXPECTED_CVES_OPTIONAL[@]} -eq 0 ]]; then
        echo "| No optional vulnerabilities"
    fi
    echo

    exit 0
else

    echo
    echo -e "${RED}Failed${NC} - Vulnerabilities from $REPORTJSON do not match expected."
    echo "|   Wanted between $FAULT_COUNT_MIN and $FAULT_COUNT_MAX, got $ACTUAL_COUNT_FULL"
    echo "|"
    echo "| vv Required: vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv"
    for ((i = 0 ; i < ${#EXPECTED_CVES_REQUIRED[@]} ; i++)); do
        echo "| ${EXPECTED_CVES_REQUIRED[$i]}"
    done
    if [[ ${#EXPECTED_CVES_REQUIRED[@]} -eq 0 ]]; then
        echo "| No required vulnerabilities"
    fi
    echo "|"
    echo "| vv Optional: vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv"
    for ((i = 0 ; i < ${#EXPECTED_CVES_OPTIONAL[@]} ; i++)); do
        echo "| ${EXPECTED_CVES_OPTIONAL[$i]}"
    done
    if [[ ${#EXPECTED_CVES_OPTIONAL[@]} -eq 0 ]]; then
        echo "| No optional vulnerabilities"
    fi
    echo "|"
    echo "| vv Actual: vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv"
    for ((i = 0 ; i < ${#ACTUAL_CVES_FULL[@]} ; i++)); do
        echo "| ${ACTUAL_CVES_FULL[$i]}"
    done
    echo "|"
    echo "| vv Diff (required vs. actual - optional): vvvvvvvv"

    rm -f $fileExpectedRequired $fileActual

    touch $fileExpectedRequired
    for ((i = 0 ; i < ${#EXPECTED_CVES_REQUIRED[@]} ; i++)); do
        echo ${EXPECTED_CVES_REQUIRED[$i]} >> $fileExpectedRequired
    done

    # Make sure we always have a file incase actual_cves is 0 items
    touch $fileActual
    for ((i = 0 ; i < ${#ACTUAL_CVES[@]} ; i++)); do
        echo ${ACTUAL_CVES[$i]} >> $fileActual
    done

    fileDiff=$(mktemp /tmp/checkreportfaultcount.diff.XXXXXX)
    busybox diff -LRequired -LActual $fileExpectedRequired $fileActual &> $fileDiff
    IFS=$'\r\n' GLOBIGNORE='*' command eval  'DIFF_OUTPUT=($(cat $fileDiff))'

    for ((i = 0 ; i < ${#DIFF_OUTPUT[@]} ; i++)); do
        echo "| ${DIFF_OUTPUT[$i]}"
    done

    rm -f $fileExpectedRequired $fileActual $fileDiff

    echo

    exit 1
fi

# ##################################################################################

${DIR}/validate_report_assets.sh "$REPORT"
${DIR}/validate_report_schema.sh "$REPORT" "$EXPECT_REPORT_VERSION"

grep -q -E "(SecretValueToStringException|Always use \.MaskedValue or \.UnmaskedValue)" *.log
rc=$?
if [ "$rc" -eq "0" ]; then
    echo "Error, Found SecretValueToStringException in log output."
    exit 1
fi

# end
