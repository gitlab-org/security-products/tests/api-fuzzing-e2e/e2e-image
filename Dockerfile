FROM alpine:latest

RUN apk -U upgrade --no-cache \
  && apk add --no-cache \
    bash \
    curl \
    jq \
    file \
  && curl https://github.com/neilpa/yajsv/releases/download/v1.4.0/yajsv.linux.386 --silent --location --output /usr/local/bin/yajsv \
  && chmod u+x /usr/local/bin/yajsv \
  && rm -rf /var/cache/* \
  && rm -rf /root/.cache/*

COPY ./scripts /scripts

# end

